<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'specialn_dbi' );

/** MySQL database username */
define( 'DB_USER', 'specialn_dbi' );

/** MySQL database password */
define( 'DB_PASSWORD', 'specialn_dbi' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'yqe]vGZ&v(N,,M*1 &#HF28#yFZ4TKIf53}Jo[6xrs/_<T0uDne>.QwDDZR)/XGc' );
define( 'SECURE_AUTH_KEY',  '/GH>.im|tcAf$G_i>s!zb`)7[]WB$jyi4%iks|{TIUu[*a6_lfLNwUbr=6kYV;U`' );
define( 'LOGGED_IN_KEY',    'K:bT278>M5=2N1NEr,c1)0f_IX^q3A_9BJGZXQsbG&> c|E$MC4fh~luBIeAU__e' );
define( 'NONCE_KEY',        ']5ZCe!)j~=2<L(*=Yj2N;=7S]lhnYuNlI7u4]k[_wC~_-YZZ^m]<0^ySq0-.~dL7' );
define( 'AUTH_SALT',        '|cCifPpI3]Ej^wKmh1LMfC?5PXx3o-c_SLa`HG^5{>DD.:;;ObgVriWU<=}dpm/%' );
define( 'SECURE_AUTH_SALT', 'x~*1nP5c{|xk{%AR]EW %Xis/aUc0(Z)nml^a$cTPM7D,jMqcMkR9c #G*![:I4f' );
define( 'LOGGED_IN_SALT',   '~a)voGfwC0:WFUo_eT-@(b@458pbJ(.Z5Up*D>z}s1b&v6~lMg0O7LJ<Hd`I2vZ<' );
define( 'NONCE_SALT',       'P*jWX%_VO,._rho~_f@(lW(.yNtl!F#rcw_es@=d?P]+[R]];_:|4!-$QMOdu!N^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
